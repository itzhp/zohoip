package com.example.zohoip;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
String ipv6="";
    String Networkadd ="";
    String cid ="";
TextInputEditText ipv4edit;
TextInputEditText sub;
TextView one,two,network,cidr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
ipv4edit=findViewById(R.id.ipv4);
one=findViewById(R.id.textView);
two=findViewById(R.id.textView2);
        cidr=findViewById(R.id.textView4);
        network=findViewById(R.id.textView3);
sub=findViewById(R.id.subnet);

    }
    public void Covert(View view) {
        String s = ipv4edit.getText().toString();
        if (s.equals("")) {
            ipv4edit.setError("Required");
        } else {
            String[] as = s.split("\\.");
            for (int i = 0; i < as.length; i++) {
                String hexVal;
                int intVal = Integer.parseInt(as[i]);
                if (intVal >= 0 && intVal <= 0xffff) {
                    hexVal = String.format("%04x", intVal);
                } else {
                    hexVal = String.format("%08x", intVal);
                }
                if (i == 2) {
                    ipv6 = ipv6 + ":" + hexVal.substring(2, 4);
                } else {
                    ipv6 = ipv6 + hexVal.substring(2, 4);
                }
            }
            one.setText("compressed ipv6 -> ::ffff:" + ipv6);
            two.setText("expanded ipv6 -> 0000:0000:0000:0000:0000:ffff:"+ipv6);
            /*Log.i("qwertyu", "::ffff:" + ipv6);
            Log.i("qwertyu", "0000:0000:0000:0000:0000:ffff:" + ipv6);*/
        }
    }

    public void Covertnext(View view) {
        String h="";
        String s = ipv4edit.getText().toString();
        String s1 = sub.getText().toString();
        if (s.equals("")&&s1.equals("")) {
            ipv4edit.setError("Required");
            sub.setError("Required");
        } else {
            String[] as = s.split("\\.");
            String[] as1 = s1.split("\\.");
            for (int i = 0; i < as.length; i++) {
            int res=Integer.parseInt(as[i])&Integer.parseInt(as1[i]);
            Networkadd=Networkadd+h+String.valueOf(res);
            h=".";
            }
            network.setText("Network Address -> " +Networkadd);

            ////////Cidr
            String bsub="";
            for (int i =0;i<as1.length;i++){
                String l= Integer.toBinaryString(Integer.parseInt(as1[i]));
                bsub=bsub+l;

            }
            String[] vv = bsub.split("1");
                        cidr.setText("CIDR -> "+ s +"/"+String.valueOf(vv.length-1));

        }
    }

}